import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.css']
})
export class InicioComponent implements OnInit {
  
  title = 'JAVA FRONT 17 DEVELOPER';
  subtitulo:string="Curso que consta de 3 capitulos";
  
  constructor() { }

  ngOnInit(): void {
  }

}
