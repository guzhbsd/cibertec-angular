import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dir-estru',
  templateUrl: './dir-estru.component.html',
  styleUrls: ['./dir-estru.component.css']
})
export class DirEstruComponent implements OnInit {
  
  frutas = this.devuelveFrutas();
  
  devuelveFrutas(){
    return[
      {id:1, nombre: 'Manzana', precio: 20.34, valoracion:7},
      {id:2, nombre: 'Naranja', precio: 16.34, valoracion:9},
      {id:3, nombre: 'Platano', precio: 12.34, valoracion:8},
      {id:4, nombre: 'Mandarina', precio: 14.34, valoracion:6},
      {id:5, nombre: 'Durazno', precio: 18.34, valoracion:4},
      {id:6, nombre: 'Mora', precio: 24.34, valoracion:5},
    ]
  }
  
  evaluacionContinua:number = 5;
  evaluaciones=[
      {id:1, nombre: 'Curso React', dificultad:7},
      {id:2, nombre: 'Naranja', dificultad:9},
      {id:3, nombre: 'Platano', dificultad:4},
      {id:4, nombre: 'Mandarina', dificultad:6},
      {id:5, nombre: 'Durazno', dificultad:4},
      {id:6, nombre: 'Mora', dificultad:5},
    ]

  muestraSiguienteEvaluacion(){
    this.evaluacionContinua=(this.evaluacionContinua==this.evaluaciones.length-1?1:this.evaluacionContinua+1);
  }

  // evaluaDificultad(any){

  // }

  indicaMostrarFrutas:boolean = false;
  btnMostrarFrutasCaption:string = 'Mostrar frutas';
  muestraFrutas(){
    this.indicaMostrarFrutas=!this.indicaMostrarFrutas;
    this.btnMostrarFrutasCaption = this.indicaMostrarFrutas?'Ocultar frutas':'Mostrar frutas';
  }

  evaluacion:number=1;
  
  muestraSiguienteExamen(){
    this.evaluacion=this.evaluacion===5?1:this.evaluacion+1;
  }
  
  constructor() { }

  ngOnInit(): void {
  }

}
