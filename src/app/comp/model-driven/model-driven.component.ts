import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-model-driven',
  templateUrl: './model-driven.component.html',
  styleUrls: ['./model-driven.component.css']
})
export class ModelDrivenComponent implements OnInit {

  frutaForm = new FormGroup({
    nombre: new FormControl('',[Validators.required, Validators.minLength(3), Validators.maxLength(20)]),
    precio: new FormControl('', [Validators.required, Validators.pattern(/^-?\d*(\.\d{0,2})?$/)]),
  })

  validaPrecio(event:any){
    const reg=/^-?\d*(\.\d{0,2})?$/;
    let input =event.target.value+String.fromCharCode(event.charCode);
    if(!reg.test(input)){
      event.preventDefault();
    }
  }
  listaFrutas:any = [];
  onSubmit(value:any){
    console.log(value);
    this.listaFrutas.push(value);
  }

  constructor() { }

  ngOnInit(): void {
  }

}
