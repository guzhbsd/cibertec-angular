import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-pipes',
  templateUrl: './pipes.component.html',
  styleUrls: ['./pipes.component.css']
})
export class PipesComponent implements OnInit {
  descripcion:string = "Java es un lenguaje multiplataforma";
  precio:number=123.4656789;
  igv:number=0.18;
  PI:number=3.1416;
  PIv2:number=Math.PI;
  sueldo:number=2586874385;
  saldo:number=2300;
  dia:Date=new Date();
  
  constructor() { }

  ngOnInit(): void {
  }

}
