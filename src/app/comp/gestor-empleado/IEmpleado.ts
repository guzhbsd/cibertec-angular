export interface IEmpleado {
    id:number,
    nombre:string,
    apellidos:string,
    edad:number,
    cargo:string
}