import { Component, OnInit } from '@angular/core';
import { IEmpleado } from './IEmpleado';
import { EmpleadoApiService } from '../../servicio/empleado-api.service';
import { FormGroup, FormControl } from '@angular/forms';
import { Empleado } from './Empleado';

@Component({
  selector: 'app-gestor-empleado',
  templateUrl: './gestor-empleado.component.html',
  styleUrls: ['./gestor-empleado.component.css']
})
export class GestorEmpleadoComponent implements OnInit {

  empleados:IEmpleado[]=[];

  formValue=new FormGroup({
    nombre:new FormControl(),
    apellidos:new FormControl(),
    edad:new FormControl(),
    cargo:new FormControl(),
  });
  
  empObj:Empleado = new Empleado();
  muestraBtnR:boolean=true;
  muestraBtnA:boolean=false;
  
  registrar(){
    this.empObj.nombre=this.formValue.value.nombre;
    this.empObj.apellidos=this.formValue.value.apellidos;
    this.empObj.edad=this.formValue.value.edad;
    this.empObj.cargo=this.formValue.value.cargo;

    this.empleadoService.nuevoEmpleado(this.empObj).subscribe(respuesta=>{
      console.log(respuesta);
      console.log('Se registro correctamente');
      this.formValue.reset();
      this.listar();
    })
  }

  listar(){
    this.empleadoService.getEmpleados().subscribe((respuesta:any)=>{
      this.empleados=respuesta;
    })
  }

  eliminar(empleado:IEmpleado){
    this.empleadoService.deleteEmpleado(empleado.id).subscribe(respuesta=>{
      console.log(respuesta);
      console.log('Eliminado');
      this.listar();
    })
  }

  editar(empleado:IEmpleado){
    this.muestraBtnA=true;
    this.muestraBtnR=false;

    this.empObj.id=empleado.id;
    this.formValue.controls['nombre'].setValue(empleado.nombre);
    this.formValue.controls['apellidos'].setValue(empleado.apellidos);
    this.formValue.controls['edad'].setValue(empleado.edad);
    this.formValue.controls['cargo'].setValue(empleado.cargo);
  }

  actualizar(){
    this.empObj.nombre=this.formValue.value.nombre;
    this.empObj.apellidos=this.formValue.value.apellidos;
    this.empObj.edad=this.formValue.value.edad;
    this.empObj.cargo=this.formValue.value.cargo;
    this.empleadoService.updateEmpleado(this.empObj, this.empObj.id).subscribe(resp=>{
      console.log('Actualizado!!!');
      this.formValue.reset();
      this.muestraBtnA=false;
      this.muestraBtnR=true;
      this.listar();
    })
  }

  cancelar(){
    this.formValue.reset();
    this.muestraBtnA=false;
    this.muestraBtnR=true;
    this.listar();
  }

  constructor(private empleadoService:EmpleadoApiService) { 
  }

  ngOnInit(): void {
    this.listar();
  }

}
