import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-template-driven',
  templateUrl: './template-driven.component.html',
  styleUrls: ['./template-driven.component.css']
})
export class TemplateDrivenComponent implements OnInit {

  validaPrecio(event:any){
    const reg=/^-?\d*(\.\d{0,2})?$/;
    let input =event.target.value+String.fromCharCode(event.charCode);
    if(!reg.test(input)){
      event.preventDefault();
    }
  }
  
  listaFrutas:any = [];
  onSubmit(value:any){
    console.log(value);
    this.listaFrutas.push(value);
  }

  constructor() { }

  ngOnInit(): void {
  }

}
