import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { IEmpleado } from '../comp/gestor-empleado/IEmpleado';
import { map } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EmpleadoApiService {
  url="http://localhost:3001/empleados";

  constructor(private http:HttpClient) { }

  getEmpleados(){
    let header= new HttpHeaders().set('Type-content','application/json');
    return this.http.get(this.url,{
      headers:header
    })
  }

  nuevoEmpleado(data:IEmpleado){
    return this.http.post<IEmpleado>(this.url, data)
      .pipe(map((x)=>data))
  }

  deleteEmpleado(id:number){
    return this.http.delete<IEmpleado>(`${this.url}/${id}`).pipe(map((respuesta)=>respuesta))
  }

  updateEmpleado(data:IEmpleado, id:number){
    return this.http.put<IEmpleado>(`${this.url}/${id}`, data).pipe(map((respuesta)=>respuesta))
  }
}
