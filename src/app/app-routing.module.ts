import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DirAtribComponent } from './comp/dir-atrib/dir-atrib.component';
import { DirEstruComponent } from './comp/dir-estru/dir-estru.component';
import { PageNotFoundComponent } from './comp/page-not-found/page-not-found.component';
import { InicioComponent } from './comp/inicio/inicio.component';
import { TemplateDrivenComponent } from './comp/template-driven/template-driven.component';
import { PipesComponent } from './comp/pipes/pipes.component';
import { ModelDrivenComponent } from './comp/model-driven/model-driven.component';
import { GestorEmpleadoComponent } from './comp/gestor-empleado/gestor-empleado.component';

const routes: Routes = [
  {path:'directivas-atributo', component:DirAtribComponent},
  {path:'directivas-estructural', component:DirEstruComponent},
  {path:'inicio', component:InicioComponent},
  {path:'pipes', component:PipesComponent},
  {path:'template-driven', component:TemplateDrivenComponent},
  {path:'model-driven', component:ModelDrivenComponent},
  {path:'api-servicio-empleado', component:GestorEmpleadoComponent},
  {path:'**', pathMatch:"full", component:PageNotFoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
