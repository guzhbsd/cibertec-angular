import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DirAtribComponent } from './comp/dir-atrib/dir-atrib.component';
import { DirEstruComponent } from './comp/dir-estru/dir-estru.component';
import { NavegadorComponent } from './comp/navegador/navegador.component';
import { PageNotFoundComponent } from './comp/page-not-found/page-not-found.component';
import { InicioComponent } from './comp/inicio/inicio.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TemplateDrivenComponent } from './comp/template-driven/template-driven.component';
import { PipesComponent } from './comp/pipes/pipes.component';
import { ModelDrivenComponent } from './comp/model-driven/model-driven.component';
import { GestorEmpleadoComponent } from './comp/gestor-empleado/gestor-empleado.component';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    DirAtribComponent,
    DirEstruComponent,
    NavegadorComponent,
    PageNotFoundComponent,
    InicioComponent,
    TemplateDrivenComponent,
    PipesComponent,
    ModelDrivenComponent,
    GestorEmpleadoComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
